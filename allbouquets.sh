#!/bin/bash
#######################################################################################
#######################################################################################
### https://www.digital-eliteboard.com/threads/autoscript-vavoo-auf-e2.513335/
### On an idea of Clever999 modified by Demosat 09.02.2023
#######################################################################################
#######################################################################################
echo -e "\033[32m###Vavoo-Script on an idea of Clever999 modified by Demosat\033[0m"
sleep 2
echo -e "\033[32m###### thx to MasterX, giniman and Oyster for the authkey\033[0m"
sleep 2
echo -e "\033[32m###more info https://www.digital-eliteboard.com/threads/autoscript-vavoo-auf-e2.513335/\033[0m"
sleep 2
########################################################################################
############################## mod lululla ################################
########################################################################################
echo "start install..."

###Paketprüfung
echo pruefe curl
PAKET=curl
if [[ -f $(which $PAKET 2>/dev/null) ]]
    then
    echo -e "\033[32m$PAKET ist bereits installiert...\033[0m"

    else
    echo -e "\033[33m$PAKET wird installiert...\033[0m"
        opkg update && opkg install $PAKET
fi
sleep 1
vec=$(shuf -n 1 /home/vavookeys)
authkey="$(curl -k --location --request POST 'https://www.vavoo.tv/api/box/ping2' --header 'Content-Type: application/json' --data "{\"vec\": \"$vec\"}" | sed 's#^.*"signed":"##' | sed "s#\"}}##g" | sed 's/".*//')"
curl -k "https://www2.vavoo.to/live2/index?countries=all&output=json" > vavoo
echo "#EXTM3U" > index
cat vavoo | sed 's/\(}\),/\1}\n,/g' | sed 's/"url":"/"url":\n/g' | sed 's#,{\"group\":#\#EXTINF:-1 group-title=#g' | sed 's#,\"logo\":\"\",\"name\":#,#g' | sed 's/\"}.*//' | sed 's/\",\"tvg.*//' | sed 's#\",\"#\",#g'  >> index 
mv index index.m3u


# url="https://www2.vjackson.info/live2/index"
# authkey="eyJkYXRhIjoie1widGltZVwiOjI2MDk0NDA0MDEwMjksXCJ2YWxpZFVudGlsXCI6MjYwOTQ0MTAwMTAyOSxcImlwc1wiOltcIjE1NC45Mi4wLjIzXCJdLFwicnVsZXNldFwiOlwiZ3Vlc3RcIixcInZlcmlmaWVkXCI6dHJ1ZSxcImVycm9yXCI6bnVsbCxcImFwcFwiOntcInBsYXRmb3JtXCI6XCJ2YXZvb1wiLFwidmVyc2lvblwiOlwiMi42XCIsXCJzZXJpdmNlXCI6XCIxLjIuMjZcIixcIm9rXCI6dHJ1ZX0sXCJ1dWlkXCI6XCI1T2MyVkR3UmdyMjlSMmVXZTh1Zi9ZUitDOHZaOXAvdVM5eCtSY2cwS1FvPVwifSIsInNpZ25hdHVyZSI6ImFTdGJpT2U0V0gyTzBrZm9TN0VTV2JXTFk3RS9vVTN1OWJNeml2bDdKWkc1eW1HRElmam92blVlQUFpbVdzc3NHUDNOeHg5VDAvL0hnSUlPV21xMkpiUXJ4NFBlYVdQMkM5U1ZNTFA5cjMzYnNURXEvMXZWeVZ3RnBBMm00bTdrNHVpZkpablk4enBqNnNWNGdHUDBGd0NBbCszRnkrSm9zWmhGU0FXYkNUYz0ifQ=="
###Vavoo Download
# rm index.m3u
# curl -k $url --output index.m3u
###Filtern
cat index.m3u  | grep -i -A1 Germany > German.m3u
cat index.m3u  | grep -i -A1 Arabia > Arabia.m3u
cat index.m3u  | grep -i -A1 Balkans > Balkans.m3u
cat index.m3u  | grep -i -A1 France > France.m3u
cat index.m3u  | grep -i -A1 Poland > Poland.m3u
cat index.m3u  | grep -i -A1 Italy > Italy.m3u
cat index.m3u  | grep -i -A1 Portugal > Portugal.m3u
cat index.m3u  | grep -i -A1 Spain > Spain.m3u
cat index.m3u  | grep -i -A1 Turkey > Turkey.m3u
###Germany
###m3u erstellen VLC
echo "#EXTM3U" > vavoo-German.m3u
cat German.m3u  |   sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" | sed '/^#EXTINF/a#EXTVLCOPT:http-user-agent=VAVOO/2.6' >> vavoo-German.m3u
###E2 bouquet erstellen
echo "#NAME Vavoo-German" > /etc/enigma2/userbouquet.vavoo-Germany.tv
cat German.m3u | sed "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"\#User-Agent=VAVOO/2.6#g" | sed '/^#EXTINF/{h;d}; /^http/G' | sed 's#,#,\#DESCRIPTION #g' | sed 's#^.*,##' | sed 's#:#%3a#g' | sed 's#http#\#SERVICE 4097:0:0:0:0:0:0:0:0:0:http#g' |  sed '/--/d; s/#DESCRIPTION/@#DESCRIPTION/g' | sed '$!N;s/\n/ /' | sort -k 4 | sed 's/@/\n/g; s/@//g' >> /etc/enigma2/userbouquet.vavoo-Germany.tv
# cat German.m3u  |  sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" |  sed '/^#EXTINF/{h;d}; /^http/G' | sed 's#,#,\#DESCRIPTION #g' | sed 's#^.*,##' | sed 's#:#%3a#g' | sed 's#http#\#SERVICE 4097:0:0:0:0:0:0:0:0:0:http#g' | sed 's#==#==\#User-Agent=VAVOO/2.6#g' >> /etc/enigma2/userbouquet.vavoo-Germany.tv
###Arabia
###m3u erstellen VLC
echo "#EXTM3U" > vavoo-Arabia.m3u
cat Arabia.m3u  |   sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" | sed '/^#EXTINF/a#EXTVLCOPT:http-user-agent=VAVOO/2.6' >> vavoo-Arabia.m3u
###E2 bouquet erstellen
echo "#NAME Vavoo-Arabia" > /etc/enigma2/userbouquet.vavoo-Arabia.tv
cat Arabia.m3u  |  sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" |  sed '/^#EXTINF/{h;d}; /^http/G' | sed 's#,#,\#DESCRIPTION #g' | sed 's#^.*,##' | sed 's#:#%3a#g' | sed 's#http#\#SERVICE 4097:0:0:0:0:0:0:0:0:0:http#g' | sed 's#==#==\#User-Agent=VAVOO/2.6#g' >> /etc/enigma2/userbouquet.vavoo-Arabia.tv
###Balkans
###m3u erstellen VLC
echo "#EXTM3U" > vavoo-Balkans.m3u
cat Balkans.m3u  |   sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" | sed '/^#EXTINF/a#EXTVLCOPT:http-user-agent=VAVOO/2.6' >> vavoo-Balkans.m3u
###E2 bouquet erstellen
echo "#NAME Vavoo-Balkans" > /etc/enigma2/userbouquet.vavoo-Balkans.tv
cat Balkans.m3u  |  sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" |  sed '/^#EXTINF/{h;d}; /^http/G' | sed 's#,#,\#DESCRIPTION #g' | sed 's#^.*,##' | sed 's#:#%3a#g' | sed 's#http#\#SERVICE 4097:0:0:0:0:0:0:0:0:0:http#g' | sed 's#==#==\#User-Agent=VAVOO/2.6#g' >> /etc/enigma2/userbouquet.vavoo-Balkans.tv
###France
###m3u erstellen VLC
echo "#EXTM3U" > vavoo-France.m3u
cat France.m3u  |   sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" | sed '/^#EXTINF/a#EXTVLCOPT:http-user-agent=VAVOO/2.6' >> vavoo-France.m3u
###E2 bouquet erstellen
echo "#NAME Vavoo-France" > /etc/enigma2/userbouquet.vavoo-France.tv
cat France.m3u  |  sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" |  sed '/^#EXTINF/{h;d}; /^http/G' | sed 's#,#,\#DESCRIPTION #g' | sed 's#^.*,##' | sed 's#:#%3a#g' | sed 's#http#\#SERVICE 4097:0:0:0:0:0:0:0:0:0:http#g' | sed 's#==#==\#User-Agent=VAVOO/2.6#g' >> /etc/enigma2/userbouquet.vavoo-France.tv
###Poland
###m3u erstellen VLC
echo "#EXTM3U" > vavoo-Poland.m3u
cat Poland.m3u  |   sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" | sed '/^#EXTINF/a#EXTVLCOPT:http-user-agent=VAVOO/2.6' >> vavoo-Poland.m3u
###E2 bouquet erstellen
echo "#NAME Vavoo-Poland" > /etc/enigma2/userbouquet.vavoo-Poland.tv
cat Poland.m3u  |  sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" |  sed '/^#EXTINF/{h;d}; /^http/G' | sed 's#,#,\#DESCRIPTION #g' | sed 's#^.*,##' | sed 's#:#%3a#g' | sed 's#http#\#SERVICE 4097:0:0:0:0:0:0:0:0:0:http#g' | sed 's#==#==\#User-Agent=VAVOO/2.6#g' >> /etc/enigma2/userbouquet.vavoo-Poland.tv
###Italy
###m3u erstellen VLC
echo "#EXTM3U" > vavoo-Italy.m3u
cat Italy.m3u  |   sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" | sed '/^#EXTINF/a#EXTVLCOPT:http-user-agent=VAVOO/2.6' >> vavoo-Italy.m3u
###E2 bouquet erstellen
echo "#NAME Vavoo-Italy" > /etc/enigma2/userbouquet.vavoo-Italy.tv
cat Italy.m3u  |  sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" |  sed '/^#EXTINF/{h;d}; /^http/G' | sed 's#,#,\#DESCRIPTION #g' | sed 's#^.*,##' | sed 's#:#%3a#g' | sed 's#http#\#SERVICE 4097:0:0:0:0:0:0:0:0:0:http#g' | sed 's#==#==\#User-Agent=VAVOO/2.6#g' >> /etc/enigma2/userbouquet.vavoo-Italy.tv

###Portugal
###m3u erstellen VLC
echo "#EXTM3U" > vavoo-Portugal.m3u
cat Portugal.m3u  |   sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" | sed '/^#EXTINF/a#EXTVLCOPT:http-user-agent=VAVOO/2.6' >> vavoo-Portugal.m3u
###E2 bouquet erstellen
echo "#NAME Vavoo-Portugal" > /etc/enigma2/userbouquet.vavoo-Portugal.tv
cat Portugal.m3u  |  sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" |  sed '/^#EXTINF/{h;d}; /^http/G' | sed 's#,#,\#DESCRIPTION #g' | sed 's#^.*,##' | sed 's#:#%3a#g' | sed 's#http#\#SERVICE 4097:0:0:0:0:0:0:0:0:0:http#g' | sed 's#==#==\#User-Agent=VAVOO/2.6#g' >> /etc/enigma2/userbouquet.vavoo-Portugal.tv
###Spain
###m3u erstellen VLC
echo "#EXTM3U" > vavoo-Spain.m3u
cat Spain.m3u  |   sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" | sed '/^#EXTINF/a#EXTVLCOPT:http-user-agent=VAVOO/2.6' >> vavoo-Spain.m3u
###E2 bouquet erstellen
echo "#NAME Vavoo-Spain" > /etc/enigma2/userbouquet.vavoo-Spain.tv
cat Spain.m3u  |  sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" |  sed '/^#EXTINF/{h;d}; /^http/G' | sed 's#,#,\#DESCRIPTION #g' | sed 's#^.*,##' | sed 's#:#%3a#g' | sed 's#http#\#SERVICE 4097:0:0:0:0:0:0:0:0:0:http#g' | sed 's#==#==\#User-Agent=VAVOO/2.6#g' >> /etc/enigma2/userbouquet.vavoo-Spain.tv
###Turkey
###m3u erstellen VLC
echo "#EXTM3U" > vavoo-Turkey.m3u
cat Turkey.m3u  |   sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" | sed '/^#EXTINF/a#EXTVLCOPT:http-user-agent=VAVOO/2.6' >> vavoo-Turkey.m3u
###E2 bouquet erstellen
echo "#NAME Vavoo-Turkey" > /etc/enigma2/userbouquet.vavoo-Turkey.tv
cat Turkey.m3u  |  sed   "s#.ts#.ts?n=1\&b=5\&vavoo_auth="$authkey"#g" |  sed '/^#EXTINF/{h;d}; /^http/G' | sed 's#,#,\#DESCRIPTION #g' | sed 's#^.*,##' | sed 's#:#%3a#g' | sed 's#http#\#SERVICE 4097:0:0:0:0:0:0:0:0:0:http#g' | sed 's#==#==\#User-Agent=VAVOO/2.6#g' >> /etc/enigma2/userbouquet.vavoo-Turkey.tv

####bouquets.tv pruefen###
####Germany
if
cat /etc/enigma2/bouquets.tv  | grep vavoo-Germany > /dev/null 2>&1
then
echo -e Eintrag bouquets.tv vorhanden > /dev/null 2>&1
else
echo -e Eintrag bouquets.tv fehlt > /dev/null 2>&1
echo "#SERVICE 1:7:1:0:0:0:0:0:0:0:FROM BOUQUET "userbouquet.vavoo-Germany.tv" ORDER BY bouquet" |   sed 's#userbouquet.vavoo-Germany.tv#"userbouquet.vavoo-Germany.tv"#g' >> /etc/enigma2/bouquets.tv
fi
####Arabia
if
cat /etc/enigma2/bouquets.tv  | grep vavoo-Arabia > /dev/null 2>&1
then
echo -e Eintrag bouquets.tv vorhanden > /dev/null 2>&1
else
echo -e Eintrag bouquets.tv fehlt > /dev/null 2>&1
echo "#SERVICE 1:7:1:0:0:0:0:0:0:0:FROM BOUQUET "userbouquet.vavoo-Arabia.tv" ORDER BY bouquet" |   sed 's#userbouquet.vavoo-Arabia.tv#"userbouquet.vavoo-Arabia.tv"#g' >> /etc/enigma2/bouquets.tv
fi
####Balkans
if
cat /etc/enigma2/bouquets.tv  | grep vavoo-Balkans > /dev/null 2>&1
then
echo -e Eintrag bouquets.tv vorhanden > /dev/null 2>&1
else
echo -e Eintrag bouquets.tv fehlt > /dev/null 2>&1
echo "#SERVICE 1:7:1:0:0:0:0:0:0:0:FROM BOUQUET "userbouquet.vavoo-Balkans.tv" ORDER BY bouquet" |   sed 's#userbouquet.vavoo-Balkans.tv#"userbouquet.vavoo-Balkans.tv"#g' >> /etc/enigma2/bouquets.tv
fi
####France
if
cat /etc/enigma2/bouquets.tv  | grep vavoo-France > /dev/null 2>&1
then
echo -e Eintrag bouquets.tv vorhanden > /dev/null 2>&1
else
echo -e Eintrag bouquets.tv fehlt > /dev/null 2>&1
echo "#SERVICE 1:7:1:0:0:0:0:0:0:0:FROM BOUQUET "userbouquet.vavoo-France.tv" ORDER BY bouquet" |   sed 's#userbouquet.vavoo-France.tv#"userbouquet.vavoo-France.tv"#g' >> /etc/enigma2/bouquets.tv
fi
####Poland
if
cat /etc/enigma2/bouquets.tv  | grep vavoo-Poland > /dev/null 2>&1
then
echo -e Eintrag bouquets.tv vorhanden > /dev/null 2>&1
else
echo -e Eintrag bouquets.tv fehlt > /dev/null 2>&1
echo "#SERVICE 1:7:1:0:0:0:0:0:0:0:FROM BOUQUET "userbouquet.vavoo-Poland.tv" ORDER BY bouquet" |   sed 's#userbouquet.vavoo-Poland.tv#"userbouquet.vavoo-Poland.tv"#g' >> /etc/enigma2/bouquets.tv
fi
####Italy
if
cat /etc/enigma2/bouquets.tv  | grep vavoo-Italy > /dev/null 2>&1
then
echo -e Eintrag bouquets.tv vorhanden > /dev/null 2>&1
else
echo -e Eintrag bouquets.tv fehlt > /dev/null 2>&1
echo "#SERVICE 1:7:1:0:0:0:0:0:0:0:FROM BOUQUET "userbouquet.vavoo-Italy.tv" ORDER BY bouquet" |   sed 's#userbouquet.vavoo-Italy.tv#"userbouquet.vavoo-Italy.tv"#g' >> /etc/enigma2/bouquets.tv
fi

####Portugal
if
cat /etc/enigma2/bouquets.tv  | grep vavoo-Portugal > /dev/null 2>&1
then
echo -e Eintrag bouquets.tv vorhanden > /dev/null 2>&1
else
echo -e Eintrag bouquets.tv fehlt > /dev/null 2>&1
echo "#SERVICE 1:7:1:0:0:0:0:0:0:0:FROM BOUQUET "userbouquet.vavoo-Portugal.tv" ORDER BY bouquet" |   sed 's#userbouquet.vavoo-Portugal.tv#"userbouquet.vavoo-Portugal.tv"#g' >> /etc/enigma2/bouquets.tv
fi
####Spain
if
cat /etc/enigma2/bouquets.tv  | grep vavoo-Spain > /dev/null 2>&1
then
echo -e Eintrag bouquets.tv vorhanden > /dev/null 2>&1
else
echo -e Eintrag bouquets.tv fehlt > /dev/null 2>&1
echo "#SERVICE 1:7:1:0:0:0:0:0:0:0:FROM BOUQUET "userbouquet.vavoo-Spain.tv" ORDER BY bouquet" |   sed 's#userbouquet.vavoo-Spain.tv#"userbouquet.vavoo-Spain.tv"#g' >> /etc/enigma2/bouquets.tv
fi
####Turkey
if
cat /etc/enigma2/bouquets.tv  | grep vavoo-Turkey > /dev/null 2>&1
then
echo -e Eintrag bouquets.tv vorhanden > /dev/null 2>&1
else
echo -e Eintrag bouquets.tv fehlt > /dev/null 2>&1
echo "#SERVICE 1:7:1:0:0:0:0:0:0:0:FROM BOUQUET "userbouquet.vavoo-Turkey.tv" ORDER BY bouquet" |   sed 's#userbouquet.vavoo-Turkey.tv#"userbouquet.vavoo-Turkey.tv"#g' >> /etc/enigma2/bouquets.tv
fi

wget -q -qO - http://127.0.0.1/web/servicelistreload?mode=0 > /dev/null 2>&1

rm German.m3u
rm Arabia.m3u
rm Balkans.m3u
rm France.m3u
rm Poland.m3u
rm Italy.m3u
rm Portugal.m3u
rm Spain.m3u
rm Turkey.m3u

exit 0